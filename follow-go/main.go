// package main provides a simple CT log follower that is looking for substrings
// in the SANs of (pre)certificates.  All matches are saved as <index>.pem.
//
// Install:
//
//     $ go build -o log-follow
//     $ mv log-follow $GOPATH/bin/
//
// Examples where the first entry will be a (pre)certificate match:
//
//     $ log-follow --index 734604796
//     $ log-follow --index 734605273
//
// Example starting from a prior run:
//
//     $ log-follow --index $(cat index.end)
//
// The above uses default values, see --help for all options.  The following
// list of known logs may be helpful to try following a different CT log:
//
//     https://github.com/google/certificate-transparency-community-site/blob/master/docs/google/known-logs.md
//
// Warnings:
//
//     - This is a first take on a prototype, use with caution.
//     - Inclusion and consistency proofs are not checked.
//     - Signatures of found (pre)certificates are not checked.
package main

import (
	"bytes"
	"context"
	"encoding/base64"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"io/ioutil"
	"strings"
	"time"

	"github.com/google/certificate-transparency-go"
	"github.com/google/certificate-transparency-go/client"
	"github.com/google/certificate-transparency-go/jsonclient"
)

var (
	flagIndex     = flag.Uint64("index", 0, "start index to fetch entries from")
	flagLogURL    = flag.String("log-url", "https://ct.googleapis.com/logs/argon2022", "log URL that comes before /ct/v1/...")
	flagLogKey    = flag.String("log-key", "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEeIPc6fGmuBg6AJkv/z7NFckmHvf/OqmjchZJ6wm2qN200keRDg352dWpi7CHnSV51BpQYAj1CQY5JuRAwrrDwg==", "log public key in base64-encoded DER")
	flagBackoff   = flag.Duration("backoff", 15*time.Second, "duration to wait after an error occured")
	flagUserAgent = flag.String("user-agent", "www.sauteed-onions.org", "user-agent string - let log operators know who's following")
	flagMatches   = flag.String("matches", "onion", "substring to use as as the criteria for saving a certificate")
	flagLogfile   = flag.String("logfile", "", "filename to write log lines to (default nil, meaning log lines go to stderr)")
)

func main() {
	flag.Parse()
	if *flagLogfile != "" {
		logfile, err := os.OpenFile(*flagLogfile, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
		if err != nil {
			log.Fatal(err)
		}
		log.SetOutput(logfile)
		defer logfile.Close()
	}
	key, err := base64.StdEncoding.DecodeString(*flagLogKey)
	if err != nil {
		log.Fatal(err)
	}

	transport := &http.Client{}
	opt := jsonclient.Options{
		PublicKeyDER: key,
		UserAgent:    *flagUserAgent,
	}
	logClient, err := client.New(*flagLogURL, transport, opt)
	if err != nil {
		log.Fatal(err)
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	index := *flagIndex
	for {
		sth, err := logClient.GetSTH(ctx)
		if err != nil {
			log.Printf("WARNING: failed retrieving tree head, backoff=%v: %v\n", *flagBackoff, err)
			time.Sleep(*flagBackoff)
			continue
		}

		if index >= sth.TreeSize {
			//log.Printf("DEBUG: already up to date at tree size %d, backoff=%v\n", sth.TreeSize, *flagBackoff)
			time.Sleep(*flagBackoff)
			continue
		}

		for index < sth.TreeSize {
			start := index
			end := sth.TreeSize - 1

			//n := sth.TreeSize - start
			//log.Printf("DEBUG: fetching %d certs (%d %d)\n", n, start, end)
			rsp, err := logClient.GetRawEntries(ctx, int64(start), int64(end))
			if err != nil {
				log.Printf("WARNING: failed getting entries, backoff=%v: %v\n", *flagBackoff, err)
				time.Sleep(*flagBackoff)
				continue
			}

			for offset, rawEntry := range rsp.Entries {
				entry, err := ct.LogEntryFromLeaf(int64(start)+int64(offset), &rawEntry)
				if err != nil {
					log.Printf("ERROR: failed parsing leaf at index %d: %v\n", entry.Index, err)
					continue
				}

				sans, err := SANsFromLogEntry(entry)
				if err != nil {
					log.Printf("WARNING: no SANs at index %d: %v\n", entry.Index, err)
				}

				for _, san := range sans {
					if strings.Contains(san, *flagMatches) {
						if err := saveEntry(entry); err != nil {
							log.Printf("CRITICAL: failed saving match: %v", err)
							os.Exit(1)
						}

						log.Printf("INFO: found a match at index %d: %q\n", entry.Index, san)
						break
					}
				}
			}

			index += uint64(len(rsp.Entries))
			saveIndex(index)
		}
	}
}

func saveEntry(entry *ct.LogEntry) error {
	buf := bytes.NewBuffer(nil)
	buf.WriteString("-----BEGIN CERTIFICATE-----\n")
	if entry.X509Cert != nil {
		buf.WriteString(base64.StdEncoding.EncodeToString(entry.X509Cert.Raw))
	} else {
		buf.WriteString(base64.StdEncoding.EncodeToString(entry.Precert.Submitted.Data))
	}
	buf.WriteString("\n")
	buf.WriteString("-----END CERTIFICATE-----\n")

	path := fmt.Sprintf("%d.pem", entry.Index)
	return ioutil.WriteFile(path, buf.Bytes(), 0644)
}

func saveIndex(index uint64) error {
	data := []byte(fmt.Sprintf("%d", index))
	path := "index.end"
	return ioutil.WriteFile(path, data, 0644)
}
