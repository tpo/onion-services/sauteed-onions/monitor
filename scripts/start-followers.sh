#! /usr/bin/env bash
set -eu
DIR="$1"; shift

# Start a follower for each directory in DIR, starting at the current
# tree head if there's no index.end file.

basedir="$PWD"
for log in "$DIR"/*; do
    [[ ! -d "$log" ]] && continue
    cd "$log"
    [[ ! -d data ]] && mkdir data
    [[ ! -s data/index.end ]] && curl -sL $(cat url.txt)ct/v1/get-sth | jq .tree_size > data/index.end
    [[ ! -s data/index.end ]] && { echo "$log: unable to get current tree size for $(cat name.txt), skipping"; continue; }
    cd data
    follow-go --logfile log.txt --log-key $(cat ../pubkey) --log-url $(cat ../url.txt) --index $(cat index.end) &
    echo "$log: started $(cat ../name.txt)"
    cd "$basedir"
done
