#!/usr/bin/env bash
#
# Install dependencies and compile things on a fresh system for the monitor
# code.
#
# Tested on Debian Bullseye.
#

# Parameters
DIRNAME="`dirname $0`"
BASEPATH="$DIRNAME/.."
DEPENDENCIES_DEBIAN="golang"

# Debian
sudo apt-get update
sudo apt-get install -y $DEPENDENCIES_DEBIAN

# Build the monitor
cd $BASEPATH/monitor/follow-go
go build -o log-follow

if [ ! -z "$GOPATH" ]; then
  mkdir -p "$GOPATH/bin"
  mv log-follow "$GOPATH/bin/"
fi
