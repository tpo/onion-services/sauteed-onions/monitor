#! /usr/bin/env python3

import sys
import json
import os
from binascii import hexlify, unhexlify
from hashlib import sha256

JSON_FORMAT_SOMETHING_OLD = False

def writefile(dir, fn, content):
    with open(os.path.join(dir, fn), mode='x') as f:
        f.write(content)

if JSON_FORMAT_SOMETHING_OLD:
    for log in json.load(open(sys.argv[1]))['logs']:
        if log.get('disqualified_at'):
            continue
        dir = hexlify(sha256(log['description'].encode()).digest()).decode()
        if not os.path.exists(dir):
            os.mkdir(dir)
        writefile(dir, 'name.txt', '{}\n'.format(log['description']))
        writefile(dir, 'url.txt', 'https://{}\n'.format(log['url']))
        writefile(dir, 'pubkey', '{}\n'.format(log['key'])) # Text format (base64), despite filename.
else:
    # Using the format output by gitlab.torproject.org/rgdd/ct/cmd/ct-metadata@v0.0.0.
    # Usage example:
    #   ct-metadata get source -n google | jq -c '[.operators[].logs[] | select(.state.usable) | {description,url,key,log_id}]' | ./setup-logs.py
    for log in json.load(sys.stdin):
        dir = hexlify(sha256(log['description'].encode()).digest()).decode() # FIXME: consider using log_id
        if not os.path.exists(dir):
            os.mkdir(dir)
        writefile(dir, 'name.txt', log['description'])
        writefile(dir, 'url.txt', log['url'])
        writefile(dir, 'pubkey', log['key']) # Text format (base64), despite filename.
        writefile(dir, 'log_id', log['log_id']) # Text format (base64), despite filename.
