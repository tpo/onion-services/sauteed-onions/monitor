#! /usr/bin/env bash
set -eu

# example usage:
#   date; for log in ~/logs/active/*; do printf "%s (%s): %s\n" $(basename $log) "$(cat $log/name.txt)" $(print-backlog.sh $log); done

usage() {
	echo "usage: $(basename "$0") LOGDIR"
	false
}

set -eu
logdir="${1-}"; shift || usage
[ -d "$logdir" ]
last_seen=$(cat "$logdir"/data/index.end)
tree_size=$(curl -s "$(cat "$logdir"/url.txt)"ct/v1/get-sth | jq .tree_size)
diff=$(( "$tree_size" - "$last_seen" ))
echo "$diff"
