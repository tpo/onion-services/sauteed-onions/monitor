#! /usr/bin/env bash

set -eu

# For all cert files (*.pem) in a given directory, move those with a SAN matching
# ^.{56}onion\. to the db directory iff the remaining suffix also exactly matches
# another SAN in the same certificate. Use the name of the log in the new path.
# Rename all other cert files to .seen, to not have to consider them again.

# Update index file mapping all ^.{56}onion\. matches to onion name and
# path-to-pemfile, by appending to index file (ie not sorting the file).

DBDIR="$1"; shift
LOGNAME="$1"; shift
SRCDIR="$1"; shift

INDEXFILE="$DBDIR/index"

[ -d "${DBDIR}/logs/${LOGNAME}" ] || mkdir -p "${DBDIR}/logs/${LOGNAME}"

find "$SRCDIR" -name \*.pem | while read -r fn; do
    mapfile -t dns_names < <(openssl x509 -noout -ext subjectAltName -in "$fn" | tail -n +2 | sed 's/DNS://g' | tr ',' '\n' | tr -d ' ' | sed '/^$/d')
    declare -A matches
    for dns_name in "${dns_names[@]}"; do
	if [[ "$dns_name" =~ ^.{56}onion\. ]]; then
	    prefix=$(sed -n 's/^\(.\{56\}onion\.\).*$/\1/p' <<< "$dns_name")
	    suffix=${dns_name#"$prefix"}
	    matches["$prefix"]=$suffix
	fi
    done

    for dns_name in "${dns_names[@]}"; do
	for prefix in "${!matches[@]}"; do
	    if [[ "$dns_name" == "${matches[$prefix]}" ]]; then
		onion=$(sed 's/^\(.\{56\}\)onion\./\1.onion/' <<< "$prefix")
		syversan=$prefix${matches[$prefix]}
		newfn=${DBDIR}/logs/${LOGNAME}/$(basename "$fn")
		[ -f "$fn" ] && mv -n "$fn" "$newfn"
		echo "$syversan" "$onion" "$newfn" >> "$INDEXFILE"
	    fi
	done
    done

    [ -f "$fn" ] && mv -n "$fn" "${fn}.seen"
done
