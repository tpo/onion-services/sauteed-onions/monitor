This document describes how to set up a sauteed onion CT log follower
and search service on a Debian 11 (Bullseye) system.

We assume that all the parts are being run on the same
system. Breaking them apart and having the follower running on one
system while having the search service running on another system is
trivial due to the fact that they're communicating solely through
files. rsync or a network filesystem are two ways of gluing the parts
together.

A working golang (1.17 or newer) environment is assumed, with its bin
directory in PATH.

## Setting up the follower

This example sets up followers for all the logs used by Chrome (and
probably Apple). More CT logs can be found in
[all_logs_list.json](https://www.gstatic.com/ct/log_list/all_logs_list.json).

```
$ sudo useradd -md /var/lib/sauteed -s /usr/bin/bash sauteed
$ sudo -iu sauteed
$ git clone https://gitlab.torproject.org/tpo/onion-services/sauteed-onions/monitor
$ (cd monitor/follow-go && go install)
$ mkdir -p db logs/active
$ (cd logs/active; curl -sLO https://www.gstatic.com/ct/log_list/log_list.json; ~/monitor/scripts/setup-logs.py log_list.json)
$ ~/monitor/scripts/start-followers.sh logs/active
$ cat << EOF | crontab -
@reboot $HOME/monitor/scripts/start-followers.sh logs/active
*/5 * * * * $HOME/monitor/scripts/reaper-wrapper.sh
EOF
```

## Setting up the search service

As user sauteed, build and install soapi.
```
$ (cd monitor/soapi && go install)
```

As root, make soapi start automatically.
```
# cat > /etc/systemd/system/soapi.service <<EOF
[Unit]
Description=Sauteed onions API service

[Service]
Type=simple
User=sauteed
StandardError=journal
WorkingDirectory=/var/www/db
ExecStart=/var/lib/sauteed/go/bin/soapi

[Install]
WantedBy=multi-user.target
EOF
# systemctl daemon-reload
# systemctl enable --now soapi
```

We now have soapi binding to localhost:8080. Now configure a web
server to proxy to it, but not db/. Here's an example for Apache
servering https://www.sauteed-onions.org/.

```
RewriteEngine On
RewriteRule ^/db/(.*) https://www.sauteed-onions.org/db/$1 [R,L]
ProxyPass        / http://localhost:8080/
ProxyPassReverse / http://localhost:8080/
```
