package db

import (
	"bytes"
	"context"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"git.sigsum.org/sigsum-go/pkg/log"
)

// The database file is a text file on the format
//   <sauteed onion name> SP <onion name> SP <relative path to PEM> NL
//
// Note that the relative path to the cert file MUST be on the form
//   dir1/dir2/<logname>/<int>.pem
// where <logname> matches a name in 'logs' and <int> is the CT log
// index for the certificate in that log.
//
// See https://www.sauteed-onions.org/db/index for a live database
// file.

type Db struct {
	chReq    chan request
	filename string // configuration
	mtime    time.Time
	db       []record
}

func New(ctx context.Context, fn string) (*Db, error) {
	db := &Db{
		filename: fn,
	}
	go db.run(ctx)
	return db, nil
}

func (d *Db) Get(ctx context.Context, id string) ([]record, error) {
	chResp := d.sendReq(request{cmd: cmdGet, arg: id})
	defer close(chResp)
	select {
	case response := <-chResp:
		var err error
		if response == nil {
			err = fmt.Errorf("not found")
		}
		return response, err
	}
}
func (d *Db) Match(ctx context.Context, name string) ([]record, error) {
	chResp := d.sendReq(request{cmd: cmdMatch, arg: name})
	defer close(chResp)
	select {
	case response := <-chResp:
		return response, nil
	}
}

func (r *record) DomainName() string {
	i := strings.Index(r.Sauteed, ".")
	if i == -1 {
		return ""
	}
	return r.Sauteed[i+1:]
}

func (r *record) LogId() string {
	var logs = []struct {
		name string
		id   string
	}{
		{"00e949280536b5b2d2e7941bc22392f207939d3a91e0f40478d50d69b61b28f5", "s3N3B+GEUPhjhtYFqdwRCUp5LbFnDAuH3PADDnk2pZo="},
		{"018872f1ba752118237fd168d203e831f91376c6012f08c20317ac85f1491e31", "5tIxY0B3jMEQQQbXcbnOwdJA9paEhvu6hzId/R43jlA="},
		{"02cf3837dce7b1c93d6c6ff1974b08a2887b5872a0177b5435f6a692afc0fa33", "rfe++nz/EMiLnT2cHj4YarRnKV3PsQwkyoWGNOvcgoo="},
		{"088a6445aef866848739e844468dfc180690f3c0a5bf265d020573fdfe4e3d7a", "6H6nZgvCbPYALvVyXT/g4zG5OTu5L79Y6zuQSdr1Q1o="},
		{"0d8d1b21660f82b9fb9a46a8181471454698df7ef30424d1d04497188a099b8e", "TnWjJ1yaEMM4W2zU3z9S6x3w4I4bjWnAsfpksWKaOd8="},
		{"11bdc971db7de3e1d38d99adf4da1c80b146fcd909ab2ac4da3f7d07815a3dfa", "GgT/SdBUHUCv9qDDv/HYxGcvTuzuI0BomGsXQC7ciX0="},
		{"18eb62c63aae5f87652cab677b513735cab88fb7ba71cf4c73a111b8d3593ef6", "3dzKNJXX4RYF55Uy+sef+D0cUN/bADoUEnYKLKy7yCo="},
		{"1b03974203776224cdf93c9db0b7b9b594aae80509eb6bbd560831a9654b1bed", "GZgQcQnw1lIuMIDSnj9ku4NuKMz5D1KO7t/OSj8WtMo="},
		{"24c0d77bb512283fe4b931a13ed4b4e4823ad8cbf08edb0e3752cfbacb443c68", "2ra/az+1tiKfm8K7XGvocJFxbLtRhIU0vaQ9MEjX+6s="},
		{"5330772bea9d77f5563182552f104dae70fd87c110258849b5ddccaa17905b60", "tz77JN+cTbp18jnFulj0bF38Qs96nzXEnh0JgSXttJk="},
		{"550a510e6ebba9a29431129fbcc2ec2a3dce95399f7fc69b5034615796d048ad", "7s3QZNXbGs7FXLedtM0TojKHRny87N7DUUhZRnEftZs="},
		{"55c6ea0982916817189618ac0ed7a1f074727ae8479ac2ea7907aeb13e231d37", "BZwB0yDgB4QTlYBJjRF8kDJmr69yULWvO0akPhGEDUo="},
		{"5fcaa7467346e27762d1d4004ee221186e9a74ea58771d2cca39c343e9a14dd2", "4JKz/AwdyOdoNh/eYbmWTQpSeBmKctZyxLBNpW1vVAQ="},
		{"640cec3565b81ec9c0944b7ee77ec4db2a199c1e81b7b6d36b883cddd665f9d7", "Nc8ZG7+xbFe/D61MbULLu7YnICZR6j/hKu+oA8M71kw="},
		{"6b1381a9ef130ab2a2178d036a56384a0a12a97f725ee067bcaeed2ca74f35dc", "ejKMVNi3LbYg6jjgUh7phBZwMhOFTTvSK8E6V6NS61I="},
		{"6fea1f22daa353c7276c25bd6b51db8e3a452405c4e6b5d7290d47c52ffae668", "EvFONL1TckyEBhnDjz96E/jntWKHiJxtMAWE6+WGJjo="},
		{"771bae3122bb54b908026fabdc1a4f566fae4b8f5d8a1b8e50d77504695d5e51", "O1N3dT4tuYBOizBbBv5AO2fYT8P0x70ADS1yb+H61Bc="},
		{"7e820717d2899ade220e00f39fd3e0d44b2c842e83ed7988322faf692ffc5fb1", "h0+1DcAp2ZMd5XPp8omejkUzs5LTiwpGJXS/D+6y/B4="},
		{"85e0a1cfddc2312c7e90b76d516c1dbaa3a2eefc43dfced984e166bacd104b6b", "w2X5s2VPMoPHnamOk9dBj1ure+MlLJjh0vBLuetCfSM="},
		{"8c4ceee20cda0053860c2cc83be6d8540de08766fe480c5ce70ef6b0a1379069", "E0rfGrWYQgl4DG/vTHqRpBa3I0nOWFdq367ap8Kr4CI="},
		{"94f04c90ffafea4043f92367e925c687023ff21ab0c80cc95d35cc497c8d9cdd", "SLDja9qmRzQP5WoC+p0w6xxSActW3SyB2bu/qznYhHM="},
		{"9562be8803d29d1c6a65e154c14905e747fc92306a250961d6b490a99eb90360", "ouMK5EXvva2bfjjtR2d3U9eCW4SU1yteGyzEuVCkR+c="},
		{"a24a65e5deefe9ff4a77babe85386519afba034848075c83cb0bc2c282b690e6", "rxgaKNaMo+CpikycZ6sJ+Lu8IrquvLE4o6Gd0/m2Aw0="},
		{"a5b5b100acb3f4819e7dce0ba68d79d776369e6eb80ee6bc881deaea09f75b0b", "KCyL3YEP+QkSCs4W1uDsIBvqgqOkrxnZ7/tZ6D/cQmg="},
		{"acad5f62ae7b04ebeaa696c118069961fe25d7bb90132798acc17ed340cd5fe8", "UaOw9f0BeZxWbbg3eI8MpHrMGyfL956IQpoN/tSLBeU="},
		{"adae40a2011d0f1c0881f897b0fe12b42ebff9dbce521f0218c59ca7b89fa6d7", "RqVV63X6kSAwtaKJafTzfREsQXS+/Um4havy/HD+bUc="},
		{"af51b6ba70148e81262cc12b6316523f0ec5d942badfa6e14fbec6474f145120", "zPsPaoVxCWX+lZtTzumyfCLphVwNl422qX5UwP5MDbA="},
		{"b11834071d1e32b685aad20e48bb35785e0ba452ae9df31881ad24fbe27ecc1e", "VYHUwhaQNgFK6gubVzxT8MDkOHhwJQgXL6OqHQcT0ww="},
		{"bc673199dbd7e4a9498779af3dee6ec95dddcbd5ba48460d75defffdf4f0d778", "36Veq2iCTx9sre64X04+WurNohKkal6OOxLAIERcKnM="},
		{"bd32078a464341ed04f4ca1ab95b0b5dc814b8829187322f9d28dd787639885d", "KXm+8J45OSHwVnOfY6V35b5XfZxgCvj5TV0mXCVdx4Q="},
		{"c0724d12524babfcbd2bc4f49af5337992eafbbc4c7607a39472ab3b0029dbb7", "ouK/1h7eLy8HoNZObTen3GVDsMa1LqLat4r4mm31F9g="},
		{"c074f0e76e244172875b6d083df02d3b77e9615c8edf02f353fe86ce0991fe4b", "DeHyMCvTDcFAYhIJ6lUu/Ed0fLHX6TDvDkIetH5OqjQ="},
		{"cf9421e516fb231b5e615ad74fe62eb96453eed2e207d40fe122b1aaa8294599", "PxdLT9ciR1iUHWUchL4NEu2QN38fhWrrwb8ohez4ZG4="},
		{"d06bd64582cebc46bfd6c3f1f465483959aba290411c116f644c4ffdc2154efc", "b1N2rDHwMRnYmQCkURX/dxUcEdkCwQApBo2yCJo32RM="},
		{"d29588fffd521fc63028c406e2c87a8608e219c2a00d7ebd46c20d23bd2fb2ec", "QcjKsd8iRkoQxqE6CUKHXk4xixsD6+tLx2jwkGKWBvY="},
		{"d60e8cce32ad2478b552c12eefdf5289bd405049696a6680ae41a09ff7fbb12e", "6D7Q2j71BjUy51covIlryQPTy9ERa+zraeF3fW0GvW4="},
		{"d7798357eae941b7ffeafd5499e8bc7cbb8e3ca491608f3ab3f0ac0a049ae70c", "zxFW7tUufK/zh1vZaS6b6RpxZ0qwF+ysAdJbd87MOwg="},
		{"e2b5a12c85d6de0936231383a1675a8a78dfdca5520ce60197328dc5e7a8e958", "KdA6G7Z0qnEc0wNbZVfBT4qni0/oOJRJ7KRT+US9JGg="},
		{"ea484fd77a3b3e8e693f6de34077be6ea3cae0c6321efdcb49c05d006effdfb9", "KOKBOP2DIUXpqdaqdTdtg3eohRKzwH9yQUgh3L3pjGY="},
		{"ed6ac0bfdefd959a5dfd90fca1bd0089633cdc4ce48ee45fcbed820fcf5e75b5", "3+FW66oFr7WcD4ZxjajAMk6uVtlup/WlagHRwTu+Ulw="},
		{"f052dccfd7996b5f6dcc00f687613ec496cd2f2bc48c22cc312783d7f80b62c6", "fVkeEuF4KnscYWd8Xv340IdcFKBOlZ65Ay/ZDowuebg="},
		{"fa560c13388c3527106f456715950b1dda0d0b7ea59d8c4a1a9e5d2acccf4050", "dv+IPwq2+5VRwmHM9Ye6NLSkzbsp3GhCCp/mZ0xaOnQ="},
		{"Mammoth", "b1N2rDHwMRnYmQCkURX/dxUcEdkCwQApBo2yCJo32RM="},
		{"Nessie2022", "UaOw9f0BeZxWbbg3eI8MpHrMGyfL956IQpoN/tSLBeU="},
		{"Nimbus2022", "QcjKsd8iRkoQxqE6CUKHXk4xixsD6+tLx2jwkGKWBvY="},
		{"Oak2022", "36Veq2iCTx9sre64X04+WurNohKkal6OOxLAIERcKnM="},
		{"Sabre", "VYHUwhaQNgFK6gubVzxT8MDkOHhwJQgXL6OqHQcT0ww="},
		{"Yeti2022-2", "BZwB0yDgB4QTlYBJjRF8kDJmr69yULWvO0akPhGEDUo="},
	}
	parts := strings.Split(r.CertPath, "/")
	if len(parts) < 3 {
		return ""
	}
	for _, l := range logs {
		if l.name == parts[2] {
			return l.id
		}
	}
	return "" // Failure.
}

func (r *record) LogIndex() uint64 {
	parts := strings.Split(r.CertPath, "/")
	if len(parts) < 4 {
		return 0 // Failure.
	}
	ixStr := strings.TrimSuffix(parts[3], ".pem")
	ix, err := strconv.ParseUint(ixStr, 10, 64)
	if err != nil {
		return 0 // Failure.
	}
	return ix
}

////////////////////
type record struct {
	Index    uint64 // Unique id, stable over invocations
	Sauteed  string // Sauteed onion name, ie <onion>onion.<domain name>
	Onion    string // Onion name, ie <onion>.onion
	CertPath string // Path to local copy of cert, relative
}

type command int

const (
	cmdGet command = iota
	cmdMatch
)

type request struct {
	ch  chan []record
	cmd command
	arg string
}

func (d *Db) run(ctx context.Context) {
	d.chReq = make(chan request, 128)
	defer close(d.chReq)

	ticker := time.NewTicker(1 * time.Second)
	defer ticker.Stop()

	d.readFileMaybe()
	for {
		select {
		case <-ticker.C:
			d.readFileMaybe()
		case req := <-d.chReq:
			req.ch <- d.handleRequest(req)
		case <-ctx.Done():
			return
		}
	}
}

func (d *Db) handleRequest(req request) (result []record) {
	switch req.cmd {
	case cmdGet:
		id, err := strconv.Atoi(req.arg)
		if err != nil {
			log.Debug("dbGet: invalid input: %v", req.arg)
			return nil
		}
		if id < len(d.db) {
			result = append(result, d.db[id])
		}
	case cmdMatch:
		for _, r := range d.db {
			if req.arg == r.DomainName() {
				result = append(result, r)
			}
		}
	}
	return result
}

func (d *Db) sendReq(req request) chan []record {
	req.ch = make(chan []record, 1)
	d.chReq <- req
	return req.ch
}

func (d *Db) readFileMaybe() {
	fi, err := os.Stat(d.filename)
	if err != nil {
		log.Warning("stat'ing db file: %v", err)
		return
	}
	if fi.ModTime().Sub(d.mtime) == 0 {
		return
	}
	d.mtime = fi.ModTime()

	// TODO: read one line at a time and skip the splitting on \n
	buf, err := os.ReadFile(d.filename)
	if err != nil {
		log.Warning("reading db file: %v", err)
		return
	}

	for i, line := range bytes.Split(buf, []byte("\n")) {
		parts := strings.Fields(string(line))
		if len(parts) < 3 {
			continue
		}
		d.db = append(d.db, record{
			Index:    uint64(i),
			Sauteed:  string(parts[0]),
			Onion:    string(parts[1]),
			CertPath: string(parts[2]),
		})
	}
}
