package db

import (
	"os"
	"reflect"
	"testing"
)

var (
	testRec0  = record{0, "j6dxisb6emm6v4yxjiqk2pwcqqoanooqwsqqiswfhtvgvhe4f4hgyzadonion.www.satalite.org", "j6dxisb6emm6v4yxjiqk2pwcqqoanooqwsqqiswfhtvgvhe4f4hgyzad.onion", "db/logs/Oak2022/192036729.pem"}
	testRec1  = record{1, "cyigahm4clwlimo6mfl4yjie5fwfbhlbuag57xo3kimk6invht6ffradonion.www.rgdd.se", "cyigahm4clwlimo6mfl4yjie5fwfbhlbuag57xo3kimk6invht6ffrad.onion", "db/logs/Yeti2022-2/205457657.pem"}
	testRec2  = record{2, "qvrbktnwsztjnbga6yyjbwzsdjw7u5a6vsyzv6hkj75clog4pdvy4cydonion.www.sauteed-onions.org", "qvrbktnwsztjnbga6yyjbwzsdjw7u5a6vsyzv6hkj75clog4pdvy4cyd.onion", "db/logs/Mammoth/582362461.pem"}
	testRec2d = record{3, "fakeonion.www.sauteed-onions.org", "fake.onion", "db/logs/Mammoth/47114711.pem"}

	testRecs3 = []record{testRec0, testRec1, testRec2}
	testRecs4 = []record{testRec0, testRec1, testRec2, testRec2d}
)

func TestGet(t *testing.T) {
}
func TestMatch(t *testing.T) {
}

func TestDomainName(t *testing.T) {
	for _, tbl := range []struct {
		desc    string
		sauteed string
		want    string
	}{
		{"single label", "foo", ""},
		{"valid", "foo.bar.bletch", "bar.bletch"},
	} {
		r := record{Sauteed: tbl.sauteed}
		if got, want := r.DomainName(), tbl.want; got != want {
			t.Errorf("%s: got \"%s\" but wanted \"%s\"", tbl.desc, got, want)
		}
	}
}

func TestLogId(t *testing.T) {
	for _, tbl := range []struct {
		desc string
		path string
		want string
	}{
		{"not enough elements in path", "foo", ""},
		{"not a known log", "foo/bar/bletch", ""},
		{"valid without trailing stuff", "foo/bar/Oak2022", "36Veq2iCTx9sre64X04+WurNohKkal6OOxLAIERcKnM="},
		{"valid", "foo/bar/Oak2022/335611101.pem", "36Veq2iCTx9sre64X04+WurNohKkal6OOxLAIERcKnM="},
	} {
		r := record{CertPath: tbl.path}
		if got, want := r.LogId(), tbl.want; got != want {
			t.Errorf("%s: got \"%s\" but wanted \"%s\"", tbl.desc, got, want)
		}
	}
}

func TestLogIndex(t *testing.T) {
	for _, tbl := range []struct {
		desc string
		path string
		want uint64
	}{
		{"not enough elements in path", "foo", 0},
		{"not a number", "foo/bar/bletch/nan.pem", 0},
		{"valid, missing .pem", "foo/bar/bletch/4711", 4711},
		{"valid", "foo/bar/bletch/4711.pem", 4711},
	} {
		r := record{CertPath: tbl.path}
		if got, want := r.LogIndex(), tbl.want; got != want {
			t.Errorf("%s: got \"%d\" but wanted \"%d\"", tbl.desc, got, want)
		}
	}
}

func TestHandleRequest(t *testing.T) {
	var (
		rec0     = record{0, "fakeonion.foo", "bar", "bletch"}
		db       = Db{db: []record{rec0}}
		dbLarger = Db{db: testRecs4}
	)
	for _, tbl := range []struct {
		desc string
		db   Db
		cmd  command
		arg  string
		want []record
	}{
		{"invalid command", db, 0, "", nil},

		{"invalid get: bad arg", db, cmdGet, "not-a-number", nil},
		{"valid get: no hit, simply too large", db, cmdGet, "42", nil},
		{"valid get: no hit, off-by-one", dbLarger, cmdGet, "4", nil},
		{"valid get: hit 0", db, cmdGet, "0", []record{rec0}},
		{"valid get: hit 3", dbLarger, cmdGet, "3", []record{testRec2d}},

		{"valid match: no matches", db, cmdMatch, "blahonga", nil},
		{"valid match: one match", db, cmdMatch, "foo", []record{rec0}},
		{"valid match: two matches", dbLarger, cmdMatch, "www.sauteed-onions.org", []record{testRec2, testRec2d}},
	} {
		req := request{cmd: tbl.cmd, arg: tbl.arg}
		if got, want := tbl.db.handleRequest(req), tbl.want; !reflect.DeepEqual(got, want) {
			t.Errorf("%s: got %v but wanted %v", tbl.desc, got, want)
		}
	}
}

func TestReadFile(t *testing.T) {
	const (
		fn = "db_test.txt"
	)
	for _, tbl := range []struct {
		desc         string
		fileContents []byte
		want         []record
	}{
		{
			desc: "nonexistent file",
			want: []record{},
		},
		{
			desc:         "one record",
			fileContents: []byte("foo bar bletch\n"),
			want:         []record{{0, "foo", "bar", "bletch"}},
		},
		{
			desc: "three records",
			fileContents: []byte(
				"j6dxisb6emm6v4yxjiqk2pwcqqoanooqwsqqiswfhtvgvhe4f4hgyzadonion.www.satalite.org j6dxisb6emm6v4yxjiqk2pwcqqoanooqwsqqiswfhtvgvhe4f4hgyzad.onion db/logs/Oak2022/192036729.pem\n" +
					"cyigahm4clwlimo6mfl4yjie5fwfbhlbuag57xo3kimk6invht6ffradonion.www.rgdd.se cyigahm4clwlimo6mfl4yjie5fwfbhlbuag57xo3kimk6invht6ffrad.onion db/logs/Yeti2022-2/205457657.pem\n" +
					"qvrbktnwsztjnbga6yyjbwzsdjw7u5a6vsyzv6hkj75clog4pdvy4cydonion.www.sauteed-onions.org qvrbktnwsztjnbga6yyjbwzsdjw7u5a6vsyzv6hkj75clog4pdvy4cyd.onion db/logs/Mammoth/582362461.pem\n"),
			want: testRecs3,
		},
	} {
		db := Db{filename: fn}
		if tbl.fileContents != nil {
			if err := os.WriteFile(fn, tbl.fileContents, 0644); err != nil {
				t.Fatalf("write test file: %v", err)
			}
		}
		db.readFileMaybe()
		if tbl.fileContents != nil {
			os.Remove(fn)
		}
		if got, want := db.db, tbl.want; !reflect.DeepEqual(got, want) {
			t.Errorf("%s: got \"%v\" but wanted \"%v\"", tbl.desc, got, want)
		}
	}
}
