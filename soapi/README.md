soapi is an implementation of an HTTP service for querying a
[sauteed onion](https://www.sauteed-onions.org/) monitor for sauteed
onion certificates.

## Spec

The specification for version 0 (v0) of the API can be found in
[../doc/api.md](https://gitlab.torproject.org/tpo/onion-services/sauteed-onions/monitor/-/blob/main/doc/api.md).

## Try it out
```
$ curl -LOs https://www.sauteed-onions.org/db/index
$ go build
$ ./soapi &
[1] 75771
$ leguin:soapi% curl -s http://localhost:8080/search?in=www.sauteed-onions.org | json_pp
[
   {
      "domain_name" : "www.sauteed-onions.org",
      "identifiers" : [
         "10",
         "15",
         "21",
         "25",
         "55",
         "85",
         "103",
         "108",
         "124",
         "154",
         "157",
         "161",
         "173",
         "179",
         "185",
         "187",
         "219",
         "221",
         "225",
         "259",
         "261",
         "268",
         "271",
         "297",
         "303",
         "312",
         "314",
         "316",
         "323",
         "334",
         "336",
         "350",
         "357",
         "383",
         "407",
         "427",
         "436",
         "450",
         "469",
         "482",
         "782",
         "784",
         "786",
         "788",
         "1202",
         "1204",
         "1206",
         "1208",
         "1210",
         "1212",
         "1214",
         "1216"
      ],
      "onion_addr" : "qvrbktnwsztjnbga6yyjbwzsdjw7u5a6vsyzv6hkj75clog4pdvy4cyd.onion"
   }
]
$ curl -s http://localhost:8080/get?id=1216 | json_pp
{
   "cert_path" : "db/logs/94f04c90ffafea4043f92367e925c687023ff21ab0c80cc95d35cc497c8d9cdd/892003633.pem",
   "domain_name" : "www.sauteed-onions.org",
   "log_id" : "SLDja9qmRzQP5WoC+p0w6xxSActW3SyB2bu/qznYhHM=",
   "log_index" : 892003633,
   "onion_addr" : "qvrbktnwsztjnbga6yyjbwzsdjw7u5a6vsyzv6hkj75clog4pdvy4cyd.onion"
}
```
