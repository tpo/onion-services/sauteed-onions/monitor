package handler

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"gitlab.torproject.org/tpo/onion-services/sauteed-onions/monitor/soapi/pkg/db"
)

type Handler struct {
	HandlerFun func(context.Context, *db.Db, http.ResponseWriter, *http.Request) (int, error)
	Db         *db.Db
}

func (h Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(r.Context(), 10*time.Second)
	defer cancel()

	http_code, err := h.HandlerFun(ctx, h.Db, w, r)
	if err != nil {
		http.Error(w, err.Error(), http_code)
	} else if http_code != 200 {
		w.WriteHeader(http_code)
	}
}

// Response to a search request.
type respSearch struct {
	DomainName  string   `json:"domain_name"` // matches what was specified by `in`
	OnionAddr   string   `json:"onion_addr"`  // an onion address associated with `domain_name`
	Identifiers []string `json:"identifiers"` // unique identifiers to access auxiliary certificate information
}

func Search(ctx context.Context, db *db.Db, w http.ResponseWriter, r *http.Request) (int, error) {
	// TODO: consider returning something more specific than 400 on invalid input
	query, err := url.ParseQuery(r.URL.RawQuery)
	if err != nil {
		return 400, fmt.Errorf("invalid query input")
	}
	in := query.Get("in")

	recs, err := db.Match(ctx, in)
	if err != nil {
		return 500, err
	}

	rMap := make(map[string]respSearch)
	for _, r := range recs {
		var ids []string
		name := r.DomainName()
		key := r.Sauteed
		_, ok := rMap[key]
		if !ok {
			ids = []string{strconv.FormatUint(r.Index, 10)}
		} else {
			ids = append(rMap[key].Identifiers, strconv.FormatUint(r.Index, 10))
		}
		rMap[key] = respSearch{name, r.Onion, ids}
	}

	resp := make([]respSearch, 0, len(rMap))
	for _, r := range rMap {
		resp = append(resp, r)
	}
	buf, err := json.Marshal(resp)

	w.Header().Set("Content-Type", "application/json")
	if _, err := w.Write(buf); err != nil {
		return 500, fmt.Errorf("writing response: %v", err)
	}
	return 200, nil
}

type respGet struct {
	DomainName string `json:"domain_name"`
	OnionAddr  string `json:"onion_addr"` // an onion address associated with `domain_name`
	LogId      string `json:"log_id"`     // defined in RFC 6962, §3.2; base64-encoded
	LogIndex   uint64 `json:"log_index"`  // log index to locate the sauteed onion certificate
	CertPath   string `json:"cert_path"`  // relative path to `url` for downloading the sauteed onion certificate
}

func Get(ctx context.Context, db *db.Db, w http.ResponseWriter, r *http.Request) (int, error) {
	// TODO: consider returning something more specific than 400 on invalid input
	query, err := url.ParseQuery(r.URL.RawQuery)
	if err != nil {
		return 400, fmt.Errorf("invalid query input")
	}
	id := query.Get("id")

	recs, err := db.Get(ctx, id)
	if err != nil {
		return 404, err
	}

	rec := recs[0]
	buf, err := json.Marshal(respGet{
		rec.DomainName(),
		rec.Onion,
		rec.LogId(),
		rec.LogIndex(),
		rec.CertPath,
	})

	w.Header().Set("Content-Type", "application/json")
	if _, err := w.Write(buf); err != nil {
		return 500, fmt.Errorf("writing response: %v", err)
	}
	return 200, nil
}
